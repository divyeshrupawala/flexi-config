import React, { Component } from 'react';
import Flexi from './Flexi.js';

const flexiConfig = {
  items: [
    {
      "name": "personname",
      "label": "Person's Name",
      "type": "TextField",
    },
    {
      "name": "states",
      "label": "Person's state",
      "type": "DropDown",
      "values": [
      "Maharashtra",
      "Kerala",
      "Tamil Nadu"
      ]
    }
  ]
};

class App extends Component {
  constructor(props) {
    super(props);
    this.onFlexiSubmit = this.onFlexiSubmit.bind(this);
  }

  onFlexiSubmit(updatedConfig) {
    console.log(`Updated Config: ${JSON.stringify(updatedConfig)}`);
  }

  render() {
    return (
      <div>
        <Flexi onSubmit={this.onFlexiSubmit} config={flexiConfig}/>
      </div>
    );
  }
}

export default App;
