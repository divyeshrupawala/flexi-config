import React from 'react';
import InputWithLabel from './InputWithLabel';
import DropdownWithLabel from './DropdownWithLabel';

const Flexi = ({onSubmit = f => f, config}) => {
    let items = config.items;
    let keyValue = {};

    const onClickSubmit = () => {        
        onSubmit(getUpdatedConfig());
    };

    const getUpdatedConfig = () => {
        let updateItems = items.map(item => {
            item.value = keyValue[item.name];
            return item;
        });
        return updateItems;
    }

    const onChangeValue = (id, event) => {        
        keyValue[id] = event.target.value;
    }

    return(
        <div>
             <h2>Flexi Configuration Component</h2>
             {items.map( object => {
                 switch(object.type) {
                     case 'TextField':                        
                        return <InputWithLabel key={object.name} label={object.label} id={object.name} type="text" onChangeValue={onChangeValue}/>                       
                     case 'DropDown':
                        return <DropdownWithLabel key={object.name} label={object.label} id={object.name} values={object.values} onChangeValue={onChangeValue}/>                        
                    default: break;   
                 }                 
             })}

            <br/><br/>
            <div>
                <button onClick={onClickSubmit}>Submit</button>
            </div>
        </div>       
    )
}

export default Flexi;
