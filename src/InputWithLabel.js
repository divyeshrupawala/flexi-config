import React from 'react'

const InputWithLabel = ({label, id, type, onChangeValue}) => 
	(
		<div>
          <label htmlFor={id}>{label}</label><br/>
          <input type={type} id={id} onChange={event => onChangeValue(id, event)}/>		          
        </div>
	)

export default InputWithLabel	