import React from 'react'

const DropdownWithLabel = ({label, id, values, onChangeValue}) => 
	(
		<div>
            <label htmlFor={id}>{label}</label><br/>       
            <select id={id} onChange={event => onChangeValue(id, event)}>
                {values.map( v => {
                    return <option key={v} value={v}>{v}</option>
                })} 
            </select>       
        </div>
	)

export default DropdownWithLabel	